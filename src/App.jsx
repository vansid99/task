import React, {useEffect, useState, useRef, useCallback} from "react";
import shortid from "shortid";

import './App.css';

import {getSquaresArr, request} from "./components/utils/utils";

import {ModeType} from "./components/modeType/ModeType";
import {SquaresTable} from "./components/squaresTable/SquaresTable";
import {History} from "./components/history/History";


function App() {
	const [isPause, setIsPaused] = useState(true)
	const [loading, setLoading] = useState(true)
	const [modes, setModes] = useState([])
	const [currentMode, setCurrentMode] = useState(undefined)
	const [history, setHistory] = useState([])
	const [squaresArr, setSquaresArr] = useState([])

	const prevNode = useRef(undefined)

	async function fetchData() {
		try {
			const response = await request('http://demo1030918.mockable.io')

			setModes(() => {
				return Object.keys(response).map(key => ({
					id: shortid.generate(),
					name: key.split(/(?=[A-Z])/).join(" ").toLowerCase(),
					field: response[key].field
				}))
			})
		} catch (e) {
			console.error('Something went wrong', e)
		} finally {
			setLoading(false)
		}
	}

	useEffect(() => {
		fetchData()
	}, [])

	useEffect(() => {
		setHistory([])
		setSquaresArr(currentMode ? getSquaresArr(currentMode.field) : [])
	}, [currentMode])

	const handleChangeMode = id => {
		setCurrentMode(() => modes.find(mode => mode.id === id))
	}

	const handleChangePause = () => {
		setIsPaused(prev => !prev)
	}

	const handleHoverSquares = useCallback(e => {
		if (isPause) return

		const {i, j, name} = e.target.dataset

		if (name === 'square' && prevNode.current !== e.target) {
			prevNode.current = e.target

			setHistory(prev => {
				let newArr = [...prev]
				newArr.push([i, j])

				if (newArr.length > squaresArr.length) {
					newArr = newArr.slice(newArr.length - squaresArr.length, newArr.length)
				}

				return newArr
			})
		}
	}, [isPause, squaresArr])

	return (
		<div className="wrapper">
			<div className="container">
				{
					loading ? (
						<h1>Loading...</h1>
					) : (
						<>
							<div>
								<ModeType modes={modes}
													currentMode={currentMode}
													handleChangeMode={handleChangeMode}
													handleChangePause={handleChangePause}
													isPause={isPause}
								/>

								<SquaresTable squaresArr={squaresArr}
															handleHoverSquares={handleHoverSquares}
								/>
							</div>
							<History history={history}/>
						</>
					)
				}
			</div>
		</div>
	);
}

export default App;
