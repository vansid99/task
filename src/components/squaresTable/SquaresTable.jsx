import React from "react";

const getGridStyles = size => {
	return {
		gridTemplateColumns: `repeat(${size}, auto)`,
		gridTemplateRows: `repeat(${size}, auto)`
	}
}

export const SquaresTable = React.memo(({squaresArr, handleHoverSquares}) => {

	return (
		<div className={`squares__table ${!squaresArr.length ? 'd-none' : ''}`}
				 onMouseMove={e => handleHoverSquares(e)}
				 style={getGridStyles(squaresArr.length)}
		>
			{squaresArr}
		</div>
	)
})