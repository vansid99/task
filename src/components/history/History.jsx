import React from "react";

export const History = ({history}) => {

	return (
		<div className="history">
			<h2 className="history__title">History</h2>
			<div className="history__items"
			>
				{
					history.map((item, idx) =>
						<div className="history__item" key={idx}>{`row ${+item[0] + 1} col ${+item[1] + 1}`}</div>)
				}
			</div>
		</div>
	)
}