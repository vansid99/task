import {Square} from "../square/Square";

export async function request(url, method = 'GET', data = null) {
    try {
        const headers = {};
        let body;
        if (data) {
            headers['Content-Type'] = 'application/json';
            body = JSON.stringify(data);
        }

        const response = await fetch(url, {
            method,
            headers,
            body
        });

        return await response.json();
    } catch (e) {
    		throw new Error(e.message)
    }
}


export function getSquaresArr(w, h = w) {
	const arr = []

	for (let i = 0; i < w; i++) {
		const row = []

		for (let j = 0; j < h; j++) {
			row.push(<Square key={`${i}-${j}`} dataId={{i, j}}/>)
		}

		arr.push(row)
	}

	return arr
}