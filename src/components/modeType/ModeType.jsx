import React from "react";

export const ModeType = ({modes, currentMode, handleChangeMode, handleChangePause, isPause}) => {

	return (
		<div className="mode__type">
			<select className="mode__type__select"
							onChange={e => handleChangeMode(e.target.value)}
							value={currentMode ? currentMode.id : undefined}
							defaultValue='DEFAULT'>
				<option value='DEFAULT' disabled>Pick mode</option>
				{modes.map((mode, idx) => <option value={mode.id} key={idx}>{mode.name}</option>)}
			</select>
			<button className="mode__type__btn"
							disabled={!currentMode}
							onClick={handleChangePause}
							type="button">
				{!isPause ? 'Pause' : 'Start'}
			</button>
		</div>
	)
}