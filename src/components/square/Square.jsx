import React from "react";

export const Square = ({dataId: {i, j}}) => {
	return <div className="square" data-i={i} data-j={j} data-name="square"/>
}